package com.inn.appconfiguration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;
/*
@SpringBootApplication(exclude = { ThymeleafAutoConfiguration.class })
@ServletComponentScan({ "com.inn" })
@EnableScheduling
@EnableCaching
@EnableAspectJAutoProxy(proxyTargetClass = true)
@ComponentScan({ "com.inn" })
@EntityScan({ "com.inn" })
@EnableJpaRepositories({ "com.inn" })
@EnableAsync

@PropertySources(value = { @PropertySource("classpath:application.properties"),
    @PropertySource("classpath:config.properties"), })

public class AppRunner extends SpringBootServletInitializer {

 
  @Autowired
  private ApplicationContextProvider contextApplicationContextProvider;

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    return application.sources(AppRunner.class);
  }

  public static void main(String[] args) {
    ConfigUtils.setPropertiesFilePath("application.properties", "config.properties");
    SpringApplication.run(AppRunner.class);
  }}*/

@SpringBootApplication
@ComponentScan({ "com" })
@ServletComponentScan({ "com" })
@EntityScan({ "com" })
@EnableAutoConfiguration
public class AppRunner {

	public static void main(String[] args) {
		SpringApplication.run(AppRunner.class, args);
	}
}
