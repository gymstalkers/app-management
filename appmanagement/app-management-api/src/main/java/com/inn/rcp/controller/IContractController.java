package com.inn.rcp.controller;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.inn.rcp.model.Contract;

@RequestMapping(value = "/Contract")
public interface IContractController {

	@GetMapping(path = "getContract")
	public Contract getMapping(@RequestParam(required = false, name = "contractId") String contractId);

	@GetMapping(path = "findAll")
	public List<Contract> findAll();

	@PostMapping(path = "create", consumes = MediaType.APPLICATION_JSON_VALUE)
	public Contract create(@RequestBody(required = false) Contract contract);

	@PostMapping(path = "update", consumes = MediaType.APPLICATION_JSON_VALUE)
	public Contract update(@RequestBody(required = false) Contract contract);
	
	@GetMapping(path = "findById/{id}")
	public Contract findByPk(@PathVariable(required = false)Integer id) throws Exception;

	@GetMapping(path = "getContractId") 
	public Contract getContractId(@RequestParam(required = false, name = "contractId") String contractId) throws Exception;

}
