package com.inn.rcp.generic;

import java.io.Serializable;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;


public interface IGenericRepository<Entity,Pk extends Serializable> {

	List<Entity> findAll();

	@Transactional
	Entity create(Entity anEntity);
	
	@Transactional
	Entity update(Entity anEntity);

}
