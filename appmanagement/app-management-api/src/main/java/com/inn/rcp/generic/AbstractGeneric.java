package com.inn.rcp.generic;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class AbstractGeneric<Entity, Pk  extends Serializable> {

	private Class<Entity> type;

	protected AbstractGeneric(Class<Entity> type) {
		this.type = type;
	}

	private Logger logger = LogManager.getLogger(AbstractGeneric.class);

	@PersistenceContext(name = "DEFAULT", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;

	public EntityManager getEntityManager() {
		return entityManager;
	}

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public List<Entity> findAll() {
		return getEntityManager().createQuery("from " + this.getClass().getName()).getResultList();
	}

	public Entity create(Entity anEntity) {
		if (anEntity == null) {
			throw new IllegalArgumentException();
		} else {
			return this.getEntityManager().merge(anEntity);
		}
	}

	public Entity update(Entity anEntity) {
		logger.info("anEntity", anEntity);
		if (anEntity == null) {
			this.logger.warn("Cannot update a null entity.");
			throw new IllegalArgumentException();
		} else {
			return this.getEntityManager().merge(anEntity);
		}
	}
	

	public Entity findByPk(Pk entityPk) throws Exception {
		if (entityPk == null) {
			this.logger.warn("Cannot find entity[" + this.getType().getName() + "] with null primary key");
			return null;
		} else {
			return this.getEntityManager().find(this.getType(), entityPk);
		}
	}

	public Class<Entity> getType() {
		return type;
	}

	public void setType(Class<Entity> type) {
		this.type = type;
	}
}
