package com.inn.rcp.generic;

import java.io.Serializable;

public class HibernateGeneric<Entity, Pk extends Serializable> extends AbstractGeneric<Entity, Pk >
		implements IGenericRepository<Entity, Pk>  {

	public HibernateGeneric(Class<Entity> type) {
		super(type);
	}

	/*
	 * public HibernateGeneric(Class<Entity> type, EntityManager entityManager) {
	 * super(type); }
	 */
}
