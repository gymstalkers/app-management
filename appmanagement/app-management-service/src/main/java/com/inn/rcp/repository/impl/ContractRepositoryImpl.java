package com.inn.rcp.repository.impl;

import static net.logstash.logback.argument.StructuredArguments.kv;

import java.util.List;

import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.inn.rcp.generic.HibernateGeneric;
import com.inn.rcp.model.Contract;
import com.inn.rcp.repository.IContractRepository;

@Repository

public class ContractRepositoryImpl extends HibernateGeneric<Contract, Integer> implements IContractRepository {
	private Logger logger = LogManager.getLogger(ContractRepositoryImpl.class);
	private static final String INSIDE_METHOD = "Inside Method :{}";

	public ContractRepositoryImpl() {
		super(Contract.class);
	}

	public List<Contract> findAll() {
		logger.info(INSIDE_METHOD, "findAll");
		return super.findAll();
		// return super.findAll();
	}

	@Override
	public Contract create(Contract contract) {
		logger.info(INSIDE_METHOD, "create", kv("contract", contract));
		return super.create(contract);
	}

	@Override
	public Contract update(Contract contract) {
		logger.info(INSIDE_METHOD, "update", kv("contract", contract));
		return super.update(contract);
	}

	@Override
	public Contract findByPk(Integer id) throws Exception{
		logger.info(INSIDE_METHOD, "findByPk", kv("id", id));
		return super.findByPk(id);
	}

	@Override
	public Contract getContractId(String contractId) throws Exception {
		logger.info(INSIDE_METHOD, "getContractIdDetails :{}", contractId);
		try {
			Query query = getEntityManager().createNamedQuery("getContractId").setParameter("contractId", contractId);
			return (Contract) query.getSingleResult();
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

}
