package com.inn.rcp.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inn.rcp.model.Contract;
import com.inn.rcp.repository.IContractRepository;
import com.inn.rcp.service.IContractService;

@Service
public class ContractServiceImpl  implements IContractService {

	@Autowired
	private IContractRepository contractRepository;

	private Logger logger = LogManager.getLogger(ContractServiceImpl.class);

	/*
	 * @Autowired public void setDao(IContractRepository dao) { super.setDao(dao);
	 * contractRepository = dao; }
	 */

	@Override
	public List<Contract> findAll() {
		return contractRepository.findAll();
	}

	@Override
	public Contract create(Contract contract) {
		logger.info("create Service :{}", contract);
		Contract contract1 = new Contract();
		if (contract1 != null) {
			contract1.setContractDate(contract.getContractDate());
			contract1.setContractId(contract.getContractId());
			contract1.setContractName(contract.getContractName());

		} else {
			logger.info("Null Object");
		}
		return contractRepository.create(contract1);

	}
	
	@Override
	public Contract update(Contract contract) {
		/*
		 * logger.info("update Service :{}", contract); Contract contract1 = new
		 * Contract(); if (contract1 != null) {
		 * contract1.setContractDate(contract.getContractDate());
		 * contract1.setContractId(contract.getContractId());
		 * contract1.setContractName(contract.getContractName());
		 * 
		 * } else { logger.info("Null Object"); }
		 */
		return contractRepository.update(contract);

	}
	
	@Override
	public Contract getContractId(String contractId) throws Exception {
		logger.info("getContractId Service :{}", contractId);
		try {
			return contractRepository.getContractId(contractId);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	@Override
	public Contract findByPk(Integer id) throws Exception {
		logger.info("findByPk Service :{}", id);
		try {
			return contractRepository.findByPk(id);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
}
