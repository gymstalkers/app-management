package com.inn.rcp.service;

import java.util.List;

import com.inn.rcp.model.Contract;

public interface IContractService {

	public List<Contract> findAll();

	public Contract create(Contract contract);

	public Contract update(Contract contract);

	Contract getContractId(String contractId) throws Exception;

	Contract findByPk(Integer id) throws Exception;

}
