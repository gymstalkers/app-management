package com.inn.rcp.repository;

import java.util.List;

import com.inn.rcp.generic.IGenericRepository;
import com.inn.rcp.model.Contract;

public interface IContractRepository extends IGenericRepository<Contract, Integer> {

	public List<Contract> findAll();

	public Contract create(Contract contract);

	public Contract update(Contract contract);

	Contract getContractId(String contractId) throws Exception;

	public Contract findByPk(Integer id) throws Exception;

}
