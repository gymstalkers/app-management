package com.inn.rcp.controller;

import static net.logstash.logback.argument.StructuredArguments.kv;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.inn.rcp.model.Contract;
import com.inn.rcp.service.IContractService;

@RestController
public class ContractControllerImpl implements IContractController {

	private Logger logger = LogManager.getLogger(ContractControllerImpl.class);
	private static final String INSIDE_METHOD = "inside method :{}";

	@Autowired
	private IContractService contractService;

	@Override
	public Contract getMapping(String contractId) {
		logger.info(INSIDE_METHOD,"getMapping",kv("contractId", contractId));
		try {
			Contract contract = new Contract();
			contract.setContractDate(new Date());
			contract.setContractId("101");
			contract.setContractName("RCP");
			return contract;

		} catch (Exception e) {
			e.getStackTrace();
			return null;
		}

	}

	@Override
	public List<Contract> findAll() {
		return contractService.findAll();
	}

	@Override
	public Contract create(Contract contract) {
		logger.info("contract Rest :{}",contract);
		return contractService.create(contract);
	}
	@Override
	public Contract update(Contract contract) {
		logger.info("contract update Rest :{}",contract);
		return contractService.update(contract);
	}
	@Override
	public Contract getContractId(String contractId) throws Exception {
		try {
			logger.info("contract getContractId Rest :{}", contractId);
			return contractService.getContractId(contractId);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	@Override
	public Contract findByPk(Integer id) throws Exception {
		try {
			logger.info(INSIDE_METHOD,"findByPk",kv("id",id));
			return contractService.findByPk(id);
		}catch(Exception e) {
			throw new Exception(e.getMessage());
		}
	}
}
